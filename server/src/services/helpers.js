/* eslint-disable no-restricted-syntax */
const setheaders = (app) => {
  app.use('*', (res, next) => {
    console.log('app', app);
    res.setHeader('Access-Control-Expose-Headers', 'token');
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
  });
};
const monitorChangesInSource = () => {
  const production = process.env.NODE_ENV === 'production';

  if (!production) {
  // eslint-disable-next-line global-require
    const chokidar = require('chokidar');
    const watcher = chokidar.watch('./');
    watcher.on('ready', () => {
      watcher.on('all', () => {
        console.log('Clearing / module cachee from server');
        Object.keys(require.cache).forEach((id) => {
          if (/[/\\]app[/\\]/.test(id)) delete require.cache[id];
          if (/[/\\]app[/\\]/.test(id)) console.log('id', id);
        });
      });
    });
  }
};
export { setheaders, monitorChangesInSource };
