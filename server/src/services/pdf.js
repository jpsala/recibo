/* eslint-disable keyword-spacing */
/* eslint-disable no-use-before-define */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import path from 'path';
import { hostname as hname } from 'os';
import { pdfPath } from './config';
import { sendToClient } from './ws';

const del = require('del');
const fs = require('mz/fs');
const PDF2Pic = require('pdf2pic');
const pdf = require('pdf-parse');

const hostname = hname();
const { splitPDF } = require('pdf-toolz/SplitCombine');


// const fs = require('fs');

const split = async (req) => {
  const pdfFile = `${pdfPath}/recibos.pdf`;
  const destFolder = `${pdfPath}/pages`;
  console.log('reading', pdfFile);
  sendToClient({ task: 'Leyendo el pdf' }, req.userId);
  const _pdf = await fs.readFile(pdfFile);
  console.log('readed');
  sendToClient({ task: 'Dividiendo el pdf en páginas (puede tardar unos minutos)' }, req.userId);
  const pages = await splitPDF(_pdf);
  console.log('Divididas', pages);
  let error;
  const res = [];
  let idx = 0;
  sendToClient({ task: 'Grabando páginas' }, req.userId);
  for (const p of pages) {
    idx += 1;
    try {
      console.log('p', p);
      const resp = fs.writeFileSync(`${destFolder}/page-${idx}.pdf`, p);
      res.push(`${destFolder}/page-${idx}.pdf`);
      console.log('Listo página', idx, resp);
      sendToClient({ task: `Listo página ${destFolder}/page-${idx}.pdf` }, req.userId);
    } catch (_error) {
      error = _error;
      sendToClient({ task: `Error página ${destFolder}/page-${idx}.pdf . error ${error}` }, req.userId);
      console.log('error', error);
    }
  }
  return { data: res, error };
};
const savePdfAsImage = async (_pdf, img) => {
  const targetPath = `${pdfPath}/parsed`;
  console.log('pdf y img', _pdf, img);
  // await del(`${targetPath}/img.jpg`, { force: true });
  try {
    const pdf2pic = new PDF2Pic({
      density: 200, // output pixels per inch
      savename: img, // output file name
      savedir: targetPath, // output file location
      format: 'jpg', // output file format
      size: '1800x1024', // output size in pixels
    });
    await pdf2pic.convertBulk(_pdf, [1]);
    // await pdf2pic.convert(pdf);
  } catch (error) {
    console.log('error', error);
  }
};
const getUrls = async (legajo) => {
  const url = hostname === 'jpnote' ? 'localhost:8889/' : 'recibo.dyndns.org:8889/';
  const pdfParsedPath = `${pdfPath}/parsed`;
  const urls = [];
  const pdfFiles = fs.readdirSync(pdfParsedPath)
    .filter((p) => p.startsWith(`${legajo}_`))
    .filter((p) => p.includes('.pdf'));
  for(const _pdf of pdfFiles) {
    const baseName = path.basename(_pdf, '.pdf');
    const _pdfPath = `${pdfParsedPath}/${_pdf}`;
    await savePdfAsImage(_pdfPath, baseName);
    urls.push({ url: `http://${url}${_pdf}`, img: `http://${url}${baseName}_1.jpg` });
  }
  return urls;
};
const buildImages = async () => {
  // const url = 'localhost:8888/';
  const images = [];
  fs.readdirSync(`${pdfPath}/pages`)
    .filter((p) => p.includes('.pdf'))
    .forEach(async (_pdf) => {
      const baseName = path.basename(_pdf, '.pdf');
      // const imgPath = `${pdfPath}/${baseName}`;
      const _pdfPath = `${pdfPath}/${_pdf}`;
      await savePdfAsImage(_pdfPath, baseName);
      images.push(baseName);
    });
  return images;
};
const renameFiles = (files, targetDir) => {
  console.log('tar', targetDir);
  files.forEach((f) => {
    console.log('f', f);
    const targetFile = `${targetDir}/${f.datos.legajo}_${f.datos.sublegajo}.pdf`;
    fs.copyFileSync(`${f.fileName}`, targetFile);
    console.log('Copiado', targetFile);
  });
};
const parsePdf = async (fileName) => {
  const dataBuffer = fs.readFileSync(fileName);
  return pdf(dataBuffer).then((data) => {
    const text = data.text.split('\n');
    let dataFound;
    text.find((l) => {
      if (l.includes('/')) {
        const parts = l.split('/');
        if (parts.length === 2) {
          const legajo = Number(parts[0].trim());
          const sublegajo = Number(parts[1].trim());
          if (Number.isInteger(legajo) && Number.isInteger(sublegajo)) {
            console.log('detalle', `${fileName} ${legajo}/${sublegajo}`);
            dataFound = { fileName, datos: { legajo, sublegajo } };
            return true;
          }
        }
      }
      return false;
    });
    return dataFound;
  });
};
const parsePdfs = async (req) => {
  const parsedFiles = [];
  const sourceDir = `${pdfPath}/pages`;
  const targetDir = `${pdfPath}/parsed`;
  const pdfFiles = fs.readdirSync(sourceDir);
  sendToClient({ task: 'borrando páginas anteriores' }, req.userId);
  await del(`${targetDir}/*.pdf`, { force: true });
  for (const file of pdfFiles) {
    console.log('file', file);
    if (file.includes('pdf')) {
      console.log('parsing', `${sourceDir}/${file}`);
      sendToClient({ task: `${sourceDir}/${file}` }, req.userId);
      try {
        parsedFiles.push(await parsePdf(`${sourceDir}/${file}`));
      } catch (error) {
        sendToClient({ task: `error parsing  ${sourceDir}/${file} error: ${error}` }, req.userId);
        console.log('error parsing ', `${sourceDir}/${file} error: ${error}`);
      }
    }
  }
  sendToClient({ task: 'Renombrando las páginas' }, req.userId);
  renameFiles(parsedFiles, targetDir);
  return parsedFiles;
};
export { getUrls, buildImages, split, parsePdfs };
