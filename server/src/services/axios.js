import Axios from 'axios';

export default Axios.create({
  baseURL: 'http://localhost:8889',
  maxContentLength: Infinity,
  maxBodyLength: Infinity,
});
