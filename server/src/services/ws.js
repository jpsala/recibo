/* eslint-disable no-restricted-syntax */
import * as WebSocket from 'ws';

let ws;
let server;
let webSocketServer;
export const initws = (_server) => {
  server = _server;
  webSocketServer = new WebSocket.Server({ server });
  webSocketServer.on('connection', (_ws) => {
    ws = _ws;
    console.log('webSocket connected');
    ws.on('message', (m) => {
      console.log('webSocket message', m);
      const message = JSON.parse(m);
      // eslint-disable-next-line no-param-reassign
      if (message && message.id) ws.id = message.id;
      console.log('webSocketServer clients', webSocketServer.clients )
    });
  });
  return ws;
};
export const getws = () => ws;
export const getWebSocketServer = () => webSocketServer;
export const getServer = () => server;
export const sendToClient = (msg, userId) => {
  console.log('userId', userId);
  for (const client of webSocketServer.clients) {
    const { id } = client;
    if (!userId || !id) {
      console.log('Sending to all', userId, id);
      client.send(JSON.stringify(msg));
    } else if (id === userId) {
      console.log('sending to', userId);
      client.send(JSON.stringify(msg));
    }
  }
};
