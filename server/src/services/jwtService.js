import jwt from 'jsonwebtoken';
import tokenKey from '../key';

// const crypto=require('crypto');
// const bcrypt = require('bcryptjs')
const getNewToken = (id) => jwt.sign({ userId: id }, tokenKey, {
  expiresIn: 24 * 60 * 60, // expires in 24 hours
});
const verifyToken = (token) => jwt.verify(token, tokenKey, (_error, decodedToken) => {
  let error;
  if (_error) {
    error = {
      message: (_error && _error.message === 'jwt expired') ? 'La sesión expiró\nVuelva a ingresar' : _error.message,
    };
  }
  return { decodedToken, error };
});
export { getNewToken, verifyToken };
