/* eslint-disable no-restricted-syntax */
/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */
/* eslint-disable import/prefer-default-export */
import getPool from '../db';
import { getUrls } from './pdf';

const pool = getPool();
const readXlsxFile = require('read-excel-file/node');
const fs = require('fs');

const parseEmpleados = async () => {
  const file = '/mnt/shares/zoot/empleados/Empleados.xlsx';
  const empleadosExcel = await readXlsxFile(file);
  console.log('detalle', empleadosExcel);
  // return;
  let header;
  const empleados = [];
  empleadosExcel.forEach((value) => {
    if (!header) header = value;
    else {
      const fields = Object.values(value);
      const empleado = {};
      fields.forEach((field, i) => {
        empleado[header[i]] = field;
      });
      empleados.push(empleado);
    }
  });
  return empleados;
};
const importEmpleados = async () => {
  const empleados = await parseEmpleados();
  await pool.query(`
            delete from empleados
        `);
  for (const e of empleados) {
    await pool.query(`
            insert into empleados
            (persona, legajo, apellido, nombre, cuit, mail, doc)
            values(?,?,?,?,?,?,?)
        `, [
      e.Persona,
      e.Legajo,
      e.Apellido,
      e.Nombre,
      e['C.U.I.L./C.U.I.T.'],
      e['E-Mail'],
      e['Núm. Doc. 1'],
    ]);
    console.log('importado', e);
  }
  return { empleados };
};
const updateEmpleados = async () => {
  const pdfFiles = fs.readdirSync('/mnt/shares/zoot/empleados/pdf/parsed').filter((p) => p.includes('.pdf'));
  // console.log('pdfFiles', pdfFiles);
  const updated = [];
  const inserted = [];
  const errors = [];
  const [empleados] = await pool.query('select * from empleados');
  await pool.query('START TRANSACTION');
  for (const e of empleados) {
    console.log('Procesando empleado', e);
    const dni = e.doc;
    if (dni) {
      const tienePdf = pdfFiles.find((p) => p.startsWith(`${e.persona}_`));
      const [rows] = await pool.query(`select * from user u where u.documento = ${dni}`);
      if (rows.length > 0) {
        console.log('Encontrado');
        for (const empleado of rows) {
          try {
            await pool.query(`
            update user set legajo = ?, titulo=? where id = ?
            `, [e.persona, tienePdf ? 1 : 0, empleado.id]);
            console.log('updated');
            updated.push(e);
          } catch (error) {
            console.error('Error', error.message);
            errors.push(e);
          }
        }
      } else {
        console.log('No encontrado');
        try {
          await pool.query(`
          insert into user(apellido,borrado,cambiar_password,cuit,documento,email,legajo,login,nombre,password)
          values(?,?,?,?,?,?,?,?,?,?)
          `, [
            e.apellido,
            0,
            1,
            e.cuit,
            e.doc,
            e.mail,
            e.persona,
            e.doc,
            e.mail,
            e.nombre,
            e.doc,
          ]);
          console.log('inserted', e);
          inserted.push(e);
        } catch (error) {
          console.error('Error', error.message, e);
          errors.push(e);
        }
      }
    } else {
      console.log('no dni', e);
    }
  }
  await pool.query('COMMIT');
  return { empleados, updated, inserted, errors };
};
const getUserData = async (userId) => {
  const [rows] = await pool.query(`select * from user u where u.id = ${userId}`);
  return rows.length > 0 ? rows[0] : undefined;
};
// const sleep = async (milliseconds = 1000) => new Promise((resolve) => {
//   setTimeout(() => {
//     resolve('ok');
//   }, milliseconds);
// });
const getPdfUrls = async (userId) => {
  const data = await getUserData(userId);
  const resp = await getUrls(data.legajo);
  console.log('user data %O, urls %O', data, resp);
  return resp;
};

export { getPdfUrls, getUserData, updateEmpleados, importEmpleados };
