/* eslint-disable import/prefer-default-export */
import { verifyToken, getNewToken } from '../services/jwtService';

export function checkToken(req, res, next) {
  console.log('chkToken');
  const { token } = req.headers;
  if (!token || token === 'undefined') return res.status(401).send({ auth: false, message: 'Falta el token de la app.\rIngrese al sistema' });
  const { decodedToken, error } = verifyToken(token);
  if (error) {
    return res.status(401).send({ auth: false, message: error.message });
  }
  const newToken = getNewToken(decodedToken.userId);
  req.userId = decodedToken.userId;
  console.log('authMiddleware user id:', req.userId);
  res.setHeader('token', newToken);
  next();
  return undefined;
}
