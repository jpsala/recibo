import express from 'express';
import { getPdfUrls } from '../../services/empleado';

const router = express.Router();
router.get('/getPdfUrls', async (req, res) => {
  const urls = await getPdfUrls(req.userId);
  res.status(200).json({ urls });
});
router.get('/datosParaCliente', async (req, res) => {
  console.log('clients', req.app.locals.clients);
  res.status(200).json({ clients: req.app.locals.clients });
});

module.exports = router;
