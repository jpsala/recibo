/* eslint-disable no-unreachable */
/* eslint-disable no-use-before-define */
/* eslint-disable no-restricted-syntax */
import express from 'express';
// import { getPdfUrls } from '../../services/empleado';
import sh from 'shelljs';
import { sendToClient } from '../../services/ws';
import { split, parsePdfs } from '../../services/pdf';

const router = express.Router();
router.get('/', async (req, res) => {
  sendToClient({ task: 'splitting' }, req.userId);
  console.log('splitting...');
  // await sleep(4000);
  // res.status(200).json({ ok: true });
  // return;
  const { error, data } = await split(req);
  console.log('parsing...');
  sendToClient({ task: 'Parsing pages' }, req.userId);
  const parsedFiles = await parsePdfs(req);
  sendToClient({ task: 'Listo!!!' }, req.userId);
  console.log('Ok...');

  res.status(200).json({ error, data, parsedFiles });
});
router.get('/split', async (req, res) => {
  const { error, data } = await split();
  res.status(200).json({ error, data });
});
router.get('/parse', async (req, res) => {
  const parsedFiles = await parsePdfs();
  res.status(200).json(parsedFiles);
});
router.get('/scp', async (req, res) => {
  // const urls = await getPdfUrls(req.userId);
  let output;
  let error;
  try {
    output = sh.exec('pdfReader scp /mnt/shares/zoot/empleados/pdf/parsed', {
      async: false,
      silent: false,
    })
      .stdout.replace(/\t/g, '')
      .split('\n');
  } catch (_error) {
    error = _error;
    console.log('error', _error);
  }
  res.status(200).json({ error, data: output });

  res.status(200).json({ output1, output2, output3 });
});
function sleep(delay) {
  const start = new Date().getTime();
  while (new Date().getTime() < start + delay);
}
module.exports = router;
/*


*/
