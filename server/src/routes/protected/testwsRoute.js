import express from 'express';
import { sendToClient } from '../../services/ws';

const router = express.Router();
router.get('/', async (req, res) => {
  sendToClient({ task: 'splitting' }, req.userId);
  res.status(200).json({ status: 'ok' });
});
module.exports = router;
