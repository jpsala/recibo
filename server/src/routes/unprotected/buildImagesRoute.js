import express from 'express';
// import { getPdfUrls } from '../../services/empleado';
import { buildImages } from '../../services/pdf';

const router = express.Router();
router.get('/', async (req, res) => {
  // const urls = await getPdfUrls(req.userId);

  const images = await buildImages();
  res.status(200).json(images);
});

module.exports = router;
