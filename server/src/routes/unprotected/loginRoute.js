import express from 'express';
import { login } from '../../services/userService';
import { getNewToken } from '../../services/jwtService';

const router = express.Router();

// handles url http://localhost:6001/products
router.post('/', async (req, res, next) => {
  const rows = await login(req.body);
  const user = rows.length > 0 ? rows[0] : undefined;
  if (!user) {
    return res.status(401).send({ auth: false, message: 'Error de credenciales, revise los datos' });
  }
  const token = getNewToken(user.id);
  res.setHeader('token', token);
  req.userId = user.id;
  console.log('login userId', req.userId);
  res.status(200).json({
    userData: user,
  });
});


module.exports = router;
