import express from 'express';
import { updateEmpleados, importEmpleados } from '../../services/empleado';

const router = express.Router();

// handles url http://localhost:6001/products

router.get('/', async (req, res, next) => {
  // csv({ separator: ',', headers: true });
  res.send('update o import');
});
router.get('/update', async (req, res, next) => {
  // csv({ separator: ',', headers: true });
  const result = await updateEmpleados();
  console.log('result', result);
  res.status(200).json(
    result,
  );
});
router.get('/import', async (req, res, next) => {
  // csv({ separator: ',', headers: true });
  const result = await importEmpleados();
  console.log('result', result);
  res.status(200).json(
    result,
  );
});


module.exports = router;
