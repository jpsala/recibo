/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import express from 'express';
import cors from 'cors';
import bodyparser from 'body-parser';
import { checkToken } from './middleware/authMiddleware';
import { initws } from './services/ws';
// import { monitorChangesInSource } from './services/helpers';
// const ws = new WebSocket.Server({ port: 9000 });
const loader = require('require-dir');
const os = require('os');
const { createServer } = require('http');


// ws.on('connection', (c) => console.log('c', c));
const app = express();
console.log('process.env.NODE_ENV', process.env.NODE_ENV);
const hostname = os.hostname();
app.use(cors());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.use('*', (req, res, next) => {
  res.setHeader('Access-Control-Expose-Headers', 'token');
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});
// unprotected routes
const unprotectedRoutes = loader('./routes/unprotected');
for (const route in unprotectedRoutes) {
  console.log('detalle', `/${route.replace('Route', '')}`);
  app.use(`/${route.replace('Route', '')}`, unprotectedRoutes[route]);
}
// protected routes
const protectedRoutes = loader('./routes/protected');
for (const route in protectedRoutes) {
  app.use(`/${route.replace('Route', '')}`, [checkToken, protectedRoutes[route]]);
}
const staticPath = '/mnt/shares/zoot/empleados/pdf/parsed';
console.log('staticPath', staticPath);
const staticFileMiddleware = express.static(staticPath);
app.use(staticFileMiddleware);

app.use((req, res, next) => {
  const err = new Error(`Not Found ${req.path}`);
  err.status = 404;
  next(err);
});

// all other requests are not implemented.
app.use((err, req, res) => {
  res.status(err.status || 501);
  res.json({
    error: {
      code: err.status || 501,
      message: err.message,
    },
  });
});

const port = 8889;

const server = createServer(app);
initws(server);
server.listen(port, () => {
  console.log(`started on port ${hostname} on port ${port}`);
});
