import Vue from 'vue'

const moduleState = () => {
  let userData = window.localStorage.getItem('recibo.data')
  let apiToken = window.localStorage.getItem('recibo')
  if ((!userData || userData === 'undefined') || (!apiToken | apiToken === 'undefined')) {
    userData = undefined
    apiToken = undefined
  }
  return {
    apiToken,
    user: userData ? JSON.parse(userData) : undefined
  }
}

const moduleGetters = {
  loggedIn (state) {
    return Boolean(state.user)
  }
}

const moduleActions = {

  async login (context, user) {
    context.dispatch('addLoading', { id: 'login' }, { root: true })
    const ret = await Vue.prototype.$axios({ method: 'get', url: 'login', params: user })
      .then(async (response) => {
        console.log('response', response)
        context.dispatch('removeLoading', 'login', { root: true })
        if (!response) throw Error('Error de conexión')
        if (response.status !== 200) return response
        // const { userData, eventual, descuento } = response
        const { user: userData } = response
        await context.dispatch('setLoginData', userData)
        window.localStorage.setItem('recibo.data', JSON.stringify(userData))
        return response
      })
      .catch((error) => {
        throw error
      })
    return ret
  },
  logout ({ dispatch }) {
    dispatch('setApiToken', undefined)
    dispatch('setLoginData', undefined)
  },
  setApiToken ({ commit }, value) {
    commit('API_TOKEN', value)
  },
  setLoginData ({ commit, dispatch }, data) {
    commit('USER', data)
  },
  updateApiTokenFromInterceptor ({ dispatch }, value) {
    dispatch('setApiToken', value)
  }
}

const moduleMutations = {
  USER (state, data) {
    Vue.set(state, 'user', data)
  },
  API_TOKEN (state, value) {
    window.localStorage.setItem('recibo', value)
    state.apiToken = value
  }

}
export default {
  namespaced: true,
  getters: moduleGetters,
  mutations: moduleMutations,
  actions: moduleActions,
  state: moduleState
}
