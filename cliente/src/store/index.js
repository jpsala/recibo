/* eslint-disable no-shadow */
import Vue from 'vue'
import Vuex from 'vuex'

import session from './sessionModule'

Vue.use(Vuex)

const state = {
  loading: {},
  ws: undefined
}
const getters = {
  loading: state => (id) => {
    if (id) {
      const IDExists = Object.prototype.hasOwnProperty.call(state.loading, id)
      return IDExists
    }
    return Object.keys(state.loading).length > 0
  }
}
const actions = {
  addLoading ({ commit }, data) {
    if (!data) {
      data = { label: '', id: undefined }
    } else if (typeof (data) === 'string') {
      data = { id: data, label: undefined }
    }
    commit('ADD_LOADING', data)
  },
  removeLoading ({ commit }, id) {
    commit('REMOVE_LOADING', id)
  }
//   setWs ({ commit }, data) {
  // commit('WS', data)
//   }
}
const mutations = {
  ADD_LOADING (state, { label, id }) {
    Vue.set(state.loading, id, label)
  },
  REMOVE_LOADING (state, id) {
    Vue.delete(state.loading, id)
  },
  WS (state, data) {
    state.ws = data
  }
}
export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  modules: {
    session
  },
  strict: process.env.DEV
})
