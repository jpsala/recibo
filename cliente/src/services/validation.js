export default (validations) => {
  const clean = () => {
    validations.validating = false
    validations.error = false
    Object.keys(validations).forEach((k) => {
      if (typeof (validations[k]) === 'object') {
        validations[k].error = false
      }
    })
  }
  return {
    clean
  }
}
