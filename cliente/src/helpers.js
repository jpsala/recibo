import { Dialog, Notify } from 'quasar'

const alerta = async (title = 'Alerta', message = '') => {
  return new Promise((resolve, reject) => {
    Dialog.create({
      title,
      message
    }).onOk(() => {
      resolve()
    })
  })
}
const notify = ({ message = 'Falta', error = false, timeout = 3000, icon = '' } = {}) => {
  console.log(message)
  Notify.create({
    color: error ? 'red-4' : 'green-4',
    textColor: 'white',
    icon,
    message: message,
    timeout: timeout
  })
}
const soloEnDevMode = func => {
  if (process.env.NODE_ENV === 'development') func()
}
const log = (...args) => {
  if (!process.env.NODE_ENV === 'development') return
  console.warn(...args)
  console.trace()
}
const handleAxiosError = (_error) => {
  const error = {}
  if (!_error) error.statusMsg = 'Error'
  else if (_error.data && _error.data.message) error.statusMsg = _error.data.message
  if (_error.status) error.status = _error.status
  return error
}
const elIsVisible = (domElement) => {
  return new Promise(resolve => {
    const o = new IntersectionObserver(([entry]) => {
      resolve(entry.intersectionRatio === 1)
      o.disconnect()
    })
    o.observe(domElement)
  })
}
import { watch } from '@vue/composition-api'
import store from 'src/store'
const onLoggedIn = async () => {
  return new Promise((resolve) => {
    watch(() => store.getters['session/loggedIn'], async (loggedIn) => {
      console.log('detalle', loggedIn)
      if (loggedIn) resolve()
    })
  })
}
export { notify, soloEnDevMode, log, handleAxiosError, elIsVisible, onLoggedIn, alerta }
