import store from 'src/store'
import { watch } from '@vue/composition-api'
const local = ['recibo', 'localhost']
const host = local.includes(document.location.hostname)
  ? 'localhost'
  : 'iae.dyndns.org'
let ws = new WebSocket(`ws://${host}:8889`)
let timeoutCallback
const onOpen = () => {
  timeoutCallback && clearInterval(timeoutCallback)
  console.log('ws boot', 'opened')
  ws.send(JSON.stringify({ id: store.state.session.user.id }))
//   store.dispatch('setWs', ws)
}
const onError = (err) => {
  console.log('ws onError', err)
}
const onClose = (a) => {
  console.log('ws boot', 'closed', a)
  timeoutCallback && clearInterval(timeoutCallback)
  timeoutCallback = setInterval(() => {
    console.log('ws boot intentando reconectar')
    try {
      ws = new WebSocket(`ws://${host}:9000`)
      console.log('ws', ws.readyState)
      ws.onopen = onOpen
      ws.onclose = onClose
      ws.onerror = onError
    } catch (error) {
      console.log('ws service couldn\'t open')
    }
  }, 1000)
//   store.dispatch('setWs', undefined)
}
watch(() => store.getters['session/loggedIn'], (loggedIn) => {
  if (loggedIn) {
    ws.onerror = onError
    ws.onopen = onOpen
    ws.onclose = onClose
  }
})
export default ws
