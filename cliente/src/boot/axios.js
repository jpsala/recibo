import axios from 'axios'

export default async ({ Vue, store }) => {
  console.log('document.location.hostname', document.location.hostname)
  const local = ['recibo', 'localhost']
  axios.defaults.baseURL = local.includes(document.location.hostname)
    ? 'http://localhost:8888/api/iae'
    : 'http://iae.dyndns.org:8888/api/iae'
  // axios.defaults.baseURL = `https://iae.com.ar:3000/`
  axios.interceptors.response.use((response) => {
    const endPoint = response.config.url.substring(response.config.url.lastIndexOf('/') + 1)
    console.log('endpoint %O response %O', endPoint, response)
    if (response.status !== 200) throw response
    if (response.headers.token) {
      store.dispatch('session/updateApiTokenFromInterceptor', response.headers.token)
    }
    response.data.status = response.status
    return response.data
  }, (error, b) => {
    console.log('error', error, b)
    Promise.reject(error)
  })

  axios.interceptors.request.use(
    async (config) => {
      config.headers.token = store.state.session.apiToken
      return config
    },
    error => Promise.reject(error)
  )

  axios.defaults.method = 'POST'
  axios.defaults.validateStatus = function (status) {
    return status >= 200 && status <= 503
  }
  Vue.prototype.$axiosRaw = axios.create()
  Vue.prototype.$axios = axios
}
